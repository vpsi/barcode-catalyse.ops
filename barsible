#!/bin/bash
#
# This is a wrapper around ansible / ansible-playbook.
#
# Usage ("ansible" mode):
#
#   barsible -m raw all -a 'echo {{ app_dir }}'
#
# Usage ("ansible-playbook" mode):
#
#   barsible -l test
#
#
# If you are unfamiliar with Ansible, read up on it at
# - https://www.ansible.com/overview/how-ansible-works
# - https://github.com/jdauphant/awesome-ansible

cd "$(dirname "$(realpath "$0")")"

# One can override these on the command line
playbook_flags=""
ansible_flags=""

warn () {
    if [ -n "$1" ]; then
        echo "$@" >&2
    else
        cat >&2
    fi
}

fatal () {
    warn "$@"
    exit 1
}

ensure_dependencies () {
    test -d /keybase/team || warn <<NO_KEYBASE

WARNING: Keybase is not operational, cannot decipher and push secrets.

NO_KEYBASE

    which eyaml >/dev/null || warn <<NO_EYAML

WARNING: eyaml is not installed, cannot decipher and push secrets.

NO_EYAML

    (
        which ansible >/dev/null 2>&1 && \
        ansible --version | perl -e '
          $_=<>;
          die "Unable to parse Ansible version\n" unless (
            my ($version) = m/ansible (?:\[core )?(.*?)\]?\s*$/);
          $numversion = version->declare($version)->numify();
          die "Found Ansible version $version ($numversion)\n" unless $numversion >= 2.008;'
    ) || fatal "Ansible version 2.8.0 or greater required."

    if ! ansible-galaxy install -g -i -r requirements.yml >/dev/null 2>&1; then
        # Try again with full logs
        ansible-galaxy install -g -i -r requirements.yml
    fi
}

inventory_mode="test"
inventories () {
    case "$inventory_mode" in
        test) echo "-i inventory/test" ;;
        test_and_prod) echo "-i inventory/test -i inventory/prod" ;;
    esac
}

###########################################################################

mode=ansible-playbook

declare -a ansible_args
while [ "$#" -gt 0 ]; do
  case "$1" in
        --prod)
            inventory_mode="test_and_prod"
            shift ;;
        -m) mode=ansible
            ansible_args+=("-m")
            shift ;;
        *)
            ansible_args+=("$1")
            shift ;;
    esac
done

set -e

# Sighh. https://github.com/ansible/ansible/issues/32499
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

case "$mode" in
    ansible-playbook)
        ensure_dependencies
        ansible-playbook $playbook_flags $(inventories) "${ansible_args[@]}" \
                         -e "barsible_cwd=$OLDPWD" \
                         barcode-playbook.yml
        ;;
    ansible)
        ansible $(inventories) $ansible_flags "${ansible_args[@]}"
        ;;
esac
