'''
When https://github.com/ansible/ansible/pull/62713 is too little too late.
'''

import os

class FilterModule(object):
    '''
    custom jinja2 filters for paths
    '''

    def filters(self):
        return {
            'pathjoin': self.pathjoin
        }

    def pathjoin(self, first, *rest):
        rest = list(rest)
        rest.append(first)
        return os.path.join(*rest)
