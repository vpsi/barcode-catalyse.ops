# gitlab-cicd role

This role implements a fully-reuseable, application-agnostic
Continuous Integration (CI) / Continuous Deployment (CD) based on
Gitlab and Docker.

The "build" phase (continuous integration) is carried out
preferentially within Docker itself. One runs one or more **Gitlab CI
runners** as Docker containers, which are tasked with building things.
In order to be able to build more Docker containers, the CI runner
gets access to `/var/run/docker.sock`.

## Variables

The following variables ought to be defined by code that invokes the role:

- `gitlab_ci_token`
- `gitlab_url`
